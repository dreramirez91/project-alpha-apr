from django.shortcuts import render, redirect, get_object_or_404
from .models import Task
from django.contrib.auth.decorators import login_required
from .forms import CreateTask
from projects.models import Project
from django.contrib.auth.models import User


@login_required
def create_task(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()
        form.fields["project"].queryset = Project.objects.filter(
            owner=request.user
        )
    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def list_tasks(request):
    user_tasks = Task.objects.filter(assignee=request.user)
    context = {
        "user_tasks": user_tasks,
    }
    return render(request, "tasks/list_tasks.html", context)


@login_required
def edit_task(request, id):
    task_details = get_object_or_404(Task, id=id)
    if request.method == "POST":
        if "delete_task" in request.POST:
            task_details.delete()
            return redirect("show_project", id=task_details.project.id)
        else:
            form = CreateTask(request.POST, instance=task_details)
            if form.is_valid():
                form.save()
                return redirect("show_project", id=task_details.project.id)
    else:
        form = CreateTask(instance=task_details)

    context = {
        "form": form,
        "task": task_details,
    }
    return render(request, "tasks/edit_task.html", context)
