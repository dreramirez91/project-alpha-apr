from django.db import models
from projects.models import Project
from django.conf import settings


class Task(models.Model):
    name = models.CharField(max_length=200)
    time_to_complete = models.PositiveSmallIntegerField(
        help_text="Number of days",
        null=True,
        blank=True,
    )
    start_date = models.DateField(help_text="YYYY-MM-DD")
    due_date = models.DateField(help_text="YYYY-MM-DD")
    notes = models.TextField(null=True, blank=True)
    is_completed = models.BooleanField(default=False)
    project = models.ForeignKey(
        Project,
        related_name="tasks",
        on_delete=models.CASCADE,
    )
    assignee = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="tasks",
        on_delete=models.CASCADE,
        null=True,
    )
