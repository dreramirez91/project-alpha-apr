from django.forms import ModelForm, Form
from .models import Task


class CreateTask(ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "time_to_complete",
            "notes",
            "is_completed",
            "project",
        )
        labels = {"start_date": "Date added", "project": "Category"}
