from datetime import datetime, date, timedelta
from django.shortcuts import render
from django.http import HttpResponse
from django.views import generic
from django.utils.safestring import mark_safe
from tasks.models import Task
from .utils import Calendar
import calendar


class CalendarView(generic.ListView):
    model = Task
    template_name = "calendar_app/calendar.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        # use today's date for the calendar
        d = get_date(self.request.GET.get("day", None))

        # Instantiate our calendar class with today's year and date
        cal = Calendar(d.year, d.month)

        # Call the formatmonth method, which returns our calendar as a table
        html_cal = cal.formatmonth(withyear=True)
        context["calendar"] = mark_safe(html_cal)

        # Add URLs for "Previous Month" and "Next Month" buttons
        prev_month_url = "?day={}".format(prev_month(d))
        next_month_url = "?day={}".format(next_month(d))
        context["prev_month_url"] = prev_month_url
        context["next_month_url"] = next_month_url

        return context


def get_date(req_day):
    if req_day:
        year, month, _ = (int(x) for x in req_day.split("-"))
        return date(year, month, day=1)
    return datetime.today()


def prev_month(d):
    first = d.replace(day=1)
    prev_month = first - timedelta(days=1)
    return prev_month.strftime("%Y-%m-%d")


def next_month(d):
    days_in_month = calendar.monthrange(d.year, d.month)[1]
    last = d.replace(day=days_in_month)
    next_month = last + timedelta(days=1)
    return next_month.strftime("%Y-%m-%d")
