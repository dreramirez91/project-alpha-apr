from datetime import datetime, timedelta
from calendar import HTMLCalendar
from tasks.models import Task


class Calendar(HTMLCalendar):
    def __init__(self, year=None, month=None):
        self.year = year
        self.month = month
        super(Calendar, self).__init__()

    # formats a day as a td
    # filter events by day
    def formatday(self, day, month, year, events):
        events_per_day = events.filter(
            due_date__day=day, due_date__month=month, due_date__year=year
        )
        d = ""
        for event in events_per_day:
            d += f"<li>{event.name}</li>"

        if day != 0:
            return f"<td><span class='date'>{day}</span><ul> {d} </ul></td>"
        return "<td></td>"

    # formats a week as a tr
    def formatweek(self, theweek, events):
        week = ""
        for d, weekday in theweek:
            events_on_day = events.filter(
                due_date__day=d,
                due_date__month=self.month,
                due_date__year=self.year,
            )
            week += self.formatday(d, self.month, self.year, events_on_day)
        return f"<tr> {week} </tr>"

    # formats a month as a table
    # filter events by year and month
    def formatmonth(self, withyear=True):
        events = Task.objects.filter(
            due_date__year=self.year, due_date__month=self.month
        )

        cal = f'<table border="0" cellpadding="0" cellspacing="0" class="calendar">\n'
        cal += f"{self.formatmonthname(self.year, self.month, withyear=withyear)}\n"
        cal += f"{self.formatweekheader()}\n"
        for week in self.monthdays2calendar(self.year, self.month):
            cal += f"{self.formatweek(week, events)}\n"
        return cal
