from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import CreateProject
from django.contrib.auth.models import User
from tasks.models import Task
import random


@login_required
def list_projects(request):
    user_projects = Project.objects.filter(owner=request.user)
    quote = random.choice(quoteList)
    context = {
        "user_projects": user_projects,
        "quote": quote,
    }
    return render(request, "projects/list_projects.html", context)


@login_required
def project_details(request, id):
    project_details = get_object_or_404(Project, id=id)
    if request.method == "POST":
        if "is_completed" in request.POST:
            task_id = request.POST.get("task_id")
            is_completed = request.POST.get("is_completed")
            task = get_object_or_404(Task, id=task_id, project=project_details)
            task.is_completed = is_completed == "True"
            task.save()
            return redirect("show_project", id=id)
        elif "delete_project" in request.POST:
            project_details.delete()
            return redirect("list_projects")
    context = {
        "project_details": project_details,
    }
    return render(request, "projects/project_details.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProject(request.POST)
        if form.is_valid():
            new_project = form.save(False)
            new_project.owner = request.user
            new_project.save()
            return redirect("list_projects")
    else:
        form = CreateProject()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)


quoteList = [
    "If you change the way you look at things, the things you look at change.",
    "Life is not a problem to be solved, but a reality to be experienced.",
    "Turn your wounds into wisdom.",
    "Everything negative – pressure, challenges – is all an opportunity for me to rise.",
    "Live for each second without hesitation.",
    "You have brains in your head. You have feet in your shoes. You can steer yourself any direction you choose.",
    "I've missed more than 9000 shots in my career. I've lost almost 300 games. 26 times I've been trusted to take the game winning shot and missed. I've failed over and over and over again in my life. And that is why I succeed.",
]
